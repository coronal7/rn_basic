import React from 'react';
import { View } from 'react-native';
import {Text, StyleSheet} from 'react-native';

const ComponentsScreen = () =>{
    const name = "Golu"
    return(
        <View>
            <Text style={styles.textStyle1}>Getting started</Text>   
            <Text style={styles.textStyle2}>My name is {name}</Text>
        </View>
    ); 
};

const styles = StyleSheet.create({
    textStyle1: {
        fontSize: 45
    },
    textStyle2: {
        fontSize: 20
    }
});

export default ComponentsScreen;