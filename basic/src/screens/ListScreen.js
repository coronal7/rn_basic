import React from 'react';
import {Text, View, StyleSheet,FlatList} from 'react-native';

const ListScreen = () =>{
    const friends = [
        {
            name: 'Friend0',
            age: 12
        },
        {
            name: 'Friend1',
            age: 12
        },
        {
            name: 'Friend2',
            age: 12
        },
        {
            name: 'Friend3',
            age: 12
        },
        {
            name: 'Friend4',
            age: 16
        },
        {
            name: 'Friend5',
            age: 14
        },
        {
            name: 'Friend6',
            age: 2
        }
    ]
    return(
        <FlatList
            //horizontal
            //showsVerticalScrollIndicator = {false}
            keyExtractor={friend => friend.name}
            data={friends}
            renderItem={({item}) =>{
            return <Text style={styles.textStyle}>{item.name} - Age {item.age}</Text>
            }}>
        </FlatList>
    ) 
};

const styles = StyleSheet.create({
    textStyle: {
        marginVertical: 50
    }
});

export default ListScreen;