import React from "react";
import { Text, StyleSheet,View,Button, TouchableOpacity } from "react-native";

const HomeScreen = (props) => {
  return(
    <View>
      <Text style={styles.text}>Home pages</Text>
      <Button
        onPress={()=> props.navigation.navigate('Components') }
        title="Components Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('List') }
        title="List Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('Image') }
        title="Image Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('Counter') }
        title="Counter Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('Color') }
        title="Color Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('Square') }
        title="Square Screen"
      />
      <Button
        onPress={()=> props.navigation.navigate('Text') }
        title="Text Screen"
      />  
      <Button
        onPress={()=> props.navigation.navigate('Box') }
        title="Box Screen"
      /> 
    </View>
    
  )
   
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;


